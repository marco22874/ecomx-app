<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Category extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'description','parent_id'];

    public function subCategory()
    {
        return $this->hasMany(Category::class, 'parent_id');
    }

    public function parent()
    {
        return $this->belongsTo(Category::class, 'parent_id');
    }

    public function scopeSort($query, $type)
    {
       $query->orderBy('order', $type);
    }

    public function scopeParent($query)
    {
        $query->whereNull('parent_id');
    }

}
