<?php

namespace App\Traits;

trait IsOrderable
{
    public function scopeSort($query, $type)
    {
       $query->orderBy('order', $type);
    }

}
