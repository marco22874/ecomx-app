<?php

namespace App\Traits;

trait IsRootCategory
{
    public function scopeParent($query)
    {
        $query->whereNull('parent_id');
    }

}
