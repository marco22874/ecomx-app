<?php

namespace Database\Factories;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;


/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Brand>
 */
class BrandFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $name = str::of(fake()->words(3,true))->ucfirst();

        return [
            'name' => $name,
            'slug' => str::of($name)->slug('-'),
            'description' => fake()->sentence(5),
            'order' => fake()->randomDigit(),
        ];
    }
}
